package BinaryTree;


public interface TreeComparator<T,K> {
    public K execute(T o);
}
