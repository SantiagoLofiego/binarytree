package BinaryTree;

import POO.Employee;
import POO.Persona;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class app {
    public static void main(String[] args) {
        Tree<Persona,Integer>arbol = new Tree<>(Persona::getAge);

        Employee emp1 = new Employee("Jorge","Lopez",25);
        Employee emp2 = new Employee("Anibal","Perez",56);
        Employee emp3 = new Employee("Mariano","Rocca",35);
        Employee emp4 = new Employee("Jael","Reyes",18);
        Employee emp5 = new Employee("Rocio","Jabit",20);
        Employee emp6 = new Employee("Inti","Ortiz",60);
        Employee emp7 = new Employee("Romina","Brusco",84);
        Employee emp8 = new Employee("Alejandro", "Badia",21);
        Employee emp9 = new Employee("Juan","Talarga",36);

        Arrays.asList(emp3,emp2,emp1,emp4,emp5,emp6,emp7,emp8,emp9).forEach(arbol::add);


        System.out.println("\n \n");
        Tree<Integer,Integer>intArbol = new Tree<>(num->num);
        Stream<Integer> numbers = Stream.generate(()-> (int) (Math.random() * 100000));
        numbers.limit(3000).forEach(intArbol::add);

        List<Integer>lista= intArbol.getGreater(-1,200,false);

        /*System.out.println("CANTIDAD DE OBJETOS OBTENIDOS: " + lista.size());
        System.out.println(lista);

        intArbol.get(p -> true).forEach(System.out::println);

        System.out.println(intArbol.getHigest());
        System.out.println(intArbol.getLowest());
        System.out.println(intArbol.size());*/

        System.out.println("Tamaño del arbol: " + intArbol.size() );
        System.out.println("Nodo mas alto= " + intArbol.getHigest());
        System.out.println("Nodo mas bajo= "+ intArbol.getLowest());
    }
}
