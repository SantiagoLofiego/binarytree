package BinaryTree;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Tree<T, K extends Comparable<K>> {
    private TreeComparator<T, K> comparator;
    private T value;
    private Tree<T, K> father;
    private Tree<T, K> leftTree;
    private Tree<T, K> rightTree;
    private Tree<T, K> root;
    private long size;
    private int aux;

    //////////////Constructors/////////////////////////
    public Tree(TreeComparator<T, K> comparator) {
        this.value = null;
        this.leftTree = null;
        this.rightTree = null;
        this.root = this;
        this.father = null;
        this.comparator = comparator;
        this.aux = 0;
        this.size = 0L;
    }

    private Tree(Tree<T, K> root) {
        this.value = null;
        this.leftTree = null;
        this.rightTree = null;
        this.root = root;
    }

    /////////////////////Methods///////////////////////////
    public boolean isEmpty() {
        return value == null;
    }

    public boolean isLeaf() {
        return value != null && leftTree == null && rightTree == null;
    }

    public void add(T content) {
        if (this.value == null) {
            this.value = content;
            root.size++;
        } else if (compare(this.value, content) < 0) {
            if (rightTree == null) rightTree = new Tree<>(this.root);
            rightTree.add(content);
            rightTree.father = this;
        } else if (compare(this.value, content) > 0) {
            if (leftTree == null) leftTree = new Tree<>(this.root);
            leftTree.add(content);
            leftTree.father = this;
        } else {
            System.out.println("No se admiten duplicados. El elemento: " + content + " ya se encuentra en el arbol");
        }
    }

    public long size() {
        return root.size;
    }

    private int compare(T A, T B) {
        return root.comparator.execute(A).compareTo(root.comparator.execute(B));
    }

    public boolean contains(T content) {
        return bSearch(content) != null;
    }

    public int count(Predicate<T> p) {
        root.aux = 0;
        List<T> list = new ArrayList<>();
        search(p, list,-1);
        return list.size();
    }

    public boolean exist(Predicate<T> p) {
        root.aux = 0;
        List<T> list = new ArrayList<>();
        search(p, list,-1);
        return list.size() > 0;
    }

    public T getLowest() {
        return minim().value;
    }

    public T getHigest() {
        return maxim().value;
    }

    public List<T> get(Predicate<T> p) {
        root.aux = 0;
        List<T> list = new ArrayList<>();
        search(p, list,-1);
        return list;
    }

    public List<T> getSome(Predicate<T> predicate, int limit) {
        root.aux = 0;
        List<T> list = new ArrayList<>();
        search(predicate,list,limit);
        return list;
    }

    public T find(K key) {
        if (root.comparator.execute(this.value).compareTo(key) == 0) {
            return this.value;
        } else if (root.comparator.execute(this.value).compareTo(key) < 0 && this.rightTree != null) {
            return this.rightTree.find(key);
        } else if (root.comparator.execute(this.value).compareTo(key) > 0 && this.leftTree != null) {
            return this.leftTree.find(key);
        } else return null;
    }

    public void remove(T content) {
        System.out.println("---------------------------------------------------------");
        System.out.println("Deleting:" + content);
        Tree<T, K> node = bSearch(content);
        if (node != null) {
            root.size--;
            if (node.leftTree != null && node.rightTree != null) {
                System.out.println("CONDITION = 2 Children");
                Tree<T, K> min = node.rightTree.minim();
                T temp = min.value;
                System.out.println("Need to remove " + temp);
                root.size++;
                remove(min.value);
                System.out.println("Continue deleting " + node.value);
                node.value = temp;
                System.out.println("RESULT TREE: ----------------------------------------------------------\n");
                root.print("-", Order.INORDER, -1);
            } else if (node == this.root && node.isLeaf()) {
                this.value = null;
                System.out.println("CONDITION = last Element");
                System.out.println("RESULT TREE: ----------------------------------------------------------\n");
                System.out.println("TREE IS EMPTY");
            } else if (node.isLeaf()) {
                if (node.father.leftTree == node) node.father.leftTree = null;
                if (node.father.rightTree == node) node.father.rightTree = null;
                node.father = null;
                node.root = null;
                System.out.println("CONDITION = 0 Children");
                System.out.println("RESULT TREE: ----------------------------------------------------------\n");
                root.print("-", Order.INORDER, -1);
            } else if (node.leftTree != null || node.rightTree != null) {
                Tree<T, K> replacement = node.leftTree != null ? node.leftTree : node.rightTree;
                node.value = replacement.value;
                node.leftTree = replacement.leftTree;
                node.rightTree = replacement.rightTree;
                if (node.leftTree != null) node.leftTree.father = node;
                if (node.rightTree != null) node.rightTree.father = node;
                System.out.println("CONDITION = 1 Children");
                System.out.println("RESULT TREE: ----------------------------------------------------------\n");
                root.print("-", Order.INORDER, -1);
            }

        } else {
            System.out.println("No existe el elemento");
        }
        System.out.println("*******************************************************");
    }

    public void removeIf(Predicate<T> p) {
        List<T> list = get(p);
        if (list.size() > 0) {
            System.out.println("DELETING " + list.size() + " Elements");
            list.forEach(System.out::println);
            list.forEach(this::remove);
        } else System.out.println("NOT MATCH FOUND FOR DELETE");
    }

    private Tree<T, K> bSearch(T content) {
        if (value != null) {
            if (compare(this.value, content) == 0) {
                return this;
            } else if (compare(this.value, content) < 0 && rightTree != null) {
                return rightTree.bSearch(content);
            } else if (compare(this.value, content) > 0 && leftTree != null) {
                return leftTree.bSearch(content);
            } else return null;
        } else {
            return null;
        }
    }

    private void search(Predicate<T> p, List<T> list, int limit) {
        if (this.value!=null) {
            if (limit <= root.aux && limit > 0) {
                return;
            }
            if (leftTree != null) leftTree.search(p, list, limit);
            if (p.test(this.value)){
                if (limit <= root.aux && limit > 0) {
                    return;
                }
                list.add(this.value);
                root.aux++;
            }
            if (rightTree != null) rightTree.search(p, list,limit);
        }
    }

    private void pathToRoot(T content, List<Tree<T,K>> list){
        if(this.value!=null)list.add(this);
        if(compare(this.value,content)>0 && leftTree != null)leftTree.pathToRoot(content,list);
        if(compare(this.value,content)<0 && rightTree != null)rightTree.pathToRoot(content,list);
    }

    public List<Tree<T,K>> getPath(T content){
        List<Tree<T,K>>list= new ArrayList<>();
        pathToRoot(content,list);
        return list;
    }

    public void preorder() {
        if (this.value != null) {
            System.out.println(this.value);
            if (leftTree != null) leftTree.preorder();
            if (rightTree != null) rightTree.preorder();
        }
    }

    private void inorder(int limit, String pref, Consumer<T> f) {
        if (this.value != null) {
            root.aux++;
            if (limit < root.aux && limit > 0) {
                return;
            }
            String pref1 = pref == null || pref.equals("") ? null : pref + pref.charAt(0) + pref.charAt(0) + "|";
            if (leftTree != null) leftTree.inorder(limit, pref1, f);
            if (pref != null) System.out.print(((pref.length() + 2) / 3 - 1) + " " + pref);
            f.accept(this.value);
            if (rightTree != null) rightTree.inorder(limit, pref1, f);
        }
    }

    public void postorder() {
        if (this.value != null) {
            if (leftTree != null) leftTree.postorder();
            if (rightTree != null) rightTree.postorder();
            System.out.println(this.value);
        }
    }

    public void reverse(int limit, String pref, Consumer<T> f) {
        if (this.value != null) {
            root.aux++;
            if (limit < root.aux && limit > 0) {
                return;
            }
            String pref1 = pref == null || pref.equals("") ? null : pref + pref.charAt(0) + pref.charAt(0) + "|";
            if (rightTree != null) rightTree.reverse(limit, pref1, f);
            if (pref != null) System.out.print(((pref.length() + 2) / 3 - 1) + " " + pref);
            f.accept(this.value);
            if (leftTree != null) leftTree.reverse(limit, pref1, f);
        }
    }

    public void print(String pref, Order order, int limit) {
        root.aux = 0;
        if (order == Order.INORDER) {
            inorder(limit, pref, System.out::println);
            System.out.println("TREE SIZE =" + this.size);
        }
        if (order == Order.REVERSE) {
            reverse(limit, pref, System.out::println);
            System.out.println("TREE SIZE =" + this.size);
        }
        if (order == Order.PREORDER) {
            inorder(limit, pref, System.out::println);
            System.out.println("TREE SIZE =" + this.size);
        }
        if (order == Order.POSTORDER) {
            inorder(limit, pref, System.out::println);
            System.out.println("TREE SIZE =" + this.size);
        }
    }

    private void descendantToList(int limit, List<T> list, T value) {
        if (limit < root.aux && limit > 0) {
            return;
        }
        boolean major = compare(this.value, value) > 0;
        if (!major && this.value != value) {
            list.add(this.value);
            root.aux++;
        }
        if (this.leftTree != null && !major) this.leftTree.reverse(limit, null, list::add);
        if (this.father != null) this.father.descendantToList(limit, list, value);
    }

    private void ascendantToList(int limit, List<T> list, T value) {
        if (limit < root.aux && limit > 0) {
            return;
        }
        boolean minor = compare(this.value, value) < 0;
        if (!minor && this.value != value) {
            list.add(this.value);
            root.aux++;
        }
        if (this.rightTree != null && !minor) this.rightTree.inorder(limit, null, list::add);
        if (this.father != null) this.father.ascendantToList(limit, list, value);
    }

    private Tree<T, K> minim() {
        if (leftTree != null) {
            return leftTree.minim();
        } else {
            return this;
        }
    }

    private Tree<T,K> maxim(){
        if(rightTree != null){
            return rightTree.maxim();
        }else {
            return this;
        }
    }

    public List<T> getGreater(int limit, T than, boolean inclusive) {
        root.aux = 0;
        List<T> list = new ArrayList<>();
        if (inclusive) list.add(than);
        Tree<T, K> node = bSearch((than));
        if (node != null) {
            node.ascendantToList(limit, list, node.value);
        }
        return list;
    }

    public List<T> getSmaller(int limit, T than, boolean inclusive) {
        root.aux = 0;
        List<T> list = new ArrayList<>();
        if (inclusive) list.add(than);
        Tree<T, K> node = bSearch((than));
        if (node != null) {
            node.descendantToList(limit, list, node.value);
            return list;
        } else {
            return null;
        }
    }

    public List<T> toList(int limit, boolean reverse) {
        root.aux = 0;
        List<T> list = new ArrayList<>();
        if (reverse) {
            reverse(limit, null, list::add);
        } else {
            inorder(limit, null, list::add);
        }
        return list;
    }

}

enum Order {
    INORDER,
    PREORDER,
    POSTORDER,
    REVERSE
}
