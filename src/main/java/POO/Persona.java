package POO;

import java.io.Serializable;

public abstract class Persona implements Serializable {
    private static int globalID =1;
    private final String name;
    private final String lastName;
    private int age;
    private final int ID;


    public Persona(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.ID = getGlobalID();
        globalID ++;
    }

    public static int getGlobalID() {
        return globalID;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public int getID() {
        return ID;
    }

    public String toString(){
        return this.resume();
    }

    public abstract String resume();
}
