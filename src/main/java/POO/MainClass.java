package POO;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;


public class MainClass {
    public static void main(String[] args) {
        System.out.println("Hola Mundo");

        /*Employee carlos = new Employee("Carlos","Lopez",45);

        Employee ana = new Employee("Ana","Juarez",34);

        Employee antonio = new Employee("Zntonio","Araoz",65);

        Persona[] employs ={ana,carlos,antonio};

        Arrays.sort(employs, Comparator.comparing(Persona::getLastName));

        for(Persona e: employs){
            System.out.println(e);
        }*/

        /*try{ObjectOutputStream guardar = new ObjectOutputStream(new FileOutputStream("prueba.txt"));
            guardar.writeObject(employs);
            guardar.close();
        }catch (Exception e){

        }*/
      Persona[] employees2={};

        try {
            ObjectInputStream abrir = new ObjectInputStream(new FileInputStream("prueba.txt"));
            employees2 = (Persona[]) abrir.readObject();

        }catch (Exception e){
            e.fillInStackTrace();
        }

        System.out.println("Imprimiendo Archivo Recuperado");
        Arrays.stream(employees2).map(Persona::resume).forEach(System.out::println);
        for(Persona e : employees2){
            System.out.println(e.resume());
        }

    }
}
