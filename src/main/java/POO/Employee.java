package POO;

public class Employee extends Persona {
    public Employee(String name, String lastName, int age) {
        super(name, lastName, age);
    }

    private int salary;

    @Override
    public String resume() {
        return "Id="+ this.getID()+ " Type= Employee" + " Name=" + this.getName()+ " "+ this.getLastName()+ " Age=" + this.getAge();
    }


    public int compareTo(Employee o) {
        if (this.getAge() == o.getAge()){
            return 0;
        }if(this.getAge() > o.getAge()){
            return -1;
        }else return 1;

    }
}
